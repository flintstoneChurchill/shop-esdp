const config = {
  output: './output',
  helpers: {
    Puppeteer: {
      url: 'http://localhost:3010',
      show: true,
      windowSize: '1200x900'
    }
  },
  include: {
    I: './steps_file.js'
  },
  mocha: {},
  bootstrap: null,
  teardown: null,
  hooks: [],
  gherkin: {
    features: './features/*.feature',
    steps: ['./step_definitions/steps.js']
  },
  plugins: {
    screenshotOnFail: {
      enabled: true
    },
    pauseOnFail: {},
    retryFailedStep: {
      enabled: true
    },
    tryTo: {
      enabled: true
    }
  },
  name: 'shop-tests'
}

if (process.profile === "chrome-ci") {
  config.helpers.Puppeteer.host =
    process.env.SELENIUM_STANDALONE_CHROME_PORT_4444_TCP_ADDR;
  // Reset config because it got overriden by BrowserStack auto detect mechanism
  config.helpers.Puppeteer.protocol = "http";
  config.helpers.Puppeteer.port = 4444;
}

exports.config = config;
