const router = require("express").Router();
const multer = require("multer");
const path = require("path");
const {nanoid} = require("nanoid");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");
const config = require("../config");
const Product = require("../models/Product");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.get("/", async (req, res) => {
  let query;
  if (req.query.category) {
    query = {category: req.query.category};
  }
  try {
    const products = await Product.find(query).populate("category", "title description");
    res.send(products);
  } catch (e) {
    res.status(500).send(e);
  }
});
router.get("/:id", async (req, res) => {
  const result = await Product.findById(req.params.id);
  if (result) {
    res.send(result);
  } else {
    res.sendStatus(404);
  }
});
router.post("/", [auth, permit("admin"), upload.single("image")], async (req, res) => {
  const productData = req.body;
  if (req.file) {
    productData.image = req.file.filename;
  }
  const product = new Product(productData);
  try {
    await product.save();
    res.send(product);
  } catch (e) {
    res.status(400).send(e);
  }
});
router.delete("/", (req, res) => {
  console.log(req.body);
  res.send(req.body)
})

module.exports = router;