# language: ru

  Функция: Регистрация пользователя
    Находясь на сайте под анонимным пользователем
    Регистрирую пользователя
    Данные должны сохраниться в базе

    Сценарий: Успешная регистрация
      Допустим я нахожусь на странице "/register"
      Если я ввожу "testUser" в поле "username"
      И я ввожу "1@345qWert" в поле "password"
      И я ввожу "testuser@test.com" в поле "email"
      И я нажимаю на кнопку "registerBtn"
      То я вижу текст "Seagate BarraCuda 1TB"