let port = 8000;
let host = "http://localhost";
if (process.env.REACT_APP_NODE_ENV === "test") {
  port = 8010;
}
if (process.env.REACT_APP_CI) {
  host = "http://group7-esdp.ddns.net";
}
if (process.env.REACT_APP_NODE_ENV === "production") {
  host = "https://group7-esdp.ddns.net";
  port = 81;
}

export const apiURL = host + ":" + port;
export const fbAppId = "670211216995860";